﻿using CalendarBE.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBE.Context.Configurations
{
    public class EventConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.ToTable("event");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Categories).WithMany(x => x.Events).HasForeignKey(x => x.CategoryId);
            builder.HasOne(x => x.Users).WithMany(x => x.Events).HasForeignKey(x => x.AppUserId);
        }
    }
}
