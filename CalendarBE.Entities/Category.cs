﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBE.Entities
{
    public class Category
    {
        [Column("id")]
        public Guid Id { set; get; }
        [Column("name")]
        public string Title { set; get; }
        [Column("colors")]
        public string Colors { get; set; }
        [Column("events")]
        public ICollection<Event> Events { get; set; }
    }
}
