﻿using CalendarBE.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBE.Entities
{
    public class Event
    {
        [Column("id")]
        public Guid Id { set; get; }
        [Column("name")]
        public string Title { set; get; }
        [Column("date")]
        public DateTime Date { set; get; }
        [Column("content")]
        public string Content { set; get; }
        
        [Column("time_start")]
        public TimeSpan TimeStart { set; get; }
        [Column("time_end")]
        public TimeSpan TimeEnd { set; get; }

        [Column("address")]
        public string Address { set; get; }
        [Column("location")]
        public string Location { set; get; }
        
        [Column("status")]
        public Status Status { set; get; }
        [Column("category_id")]
        public Guid CategoryId { set; get; }
        [Column("app_user_id")]
        public Guid AppUserId { set; get; }

        [Column("category_id")]
        public Category Categories { set; get; }

        [Column("app_user")]
        public AppUser Users { set; get; }
        
    }
}
