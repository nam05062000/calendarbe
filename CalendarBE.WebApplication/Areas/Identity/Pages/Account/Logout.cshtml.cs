﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalendarBE.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace CalendarBE.WebApplication.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ILogger<LogoutModel> _logger;

        public LogoutModel(SignInManager<AppUser> signInManager, ILogger<LogoutModel> logger)
        {
            _signInManager = signInManager;
            _logger = logger;
        }
        public string LogoutId { get; set; }
        public async Task OnGet(string logoutId = null)
        {
            await _signInManager.SignOutAsync();
        }
        public async Task<IActionResult> OnPost(string logoutId = null)
        {

            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            if (LogoutId != null)
            {
                return LocalRedirect(logoutId);
            }
            else
            {
                return Redirect("http://localhost:5885");
            }
        }
    }
}
